<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BorrowedBookRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $book = Book::where('id', request()->book_id)->firstOrFail();
        return [
            'book_id' => ['required', 'bail', 'exists:books,id'],
            'copies' => ['gt:0', 'integer', 'required', 'bail'],
            'patron_id' => ['exists:patrons,id']
        ];
    }

    public function message(){
        return [
            'book_id.exists' => 'Book id must exist on the book table',
        ];
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\PatronRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Models\Patron;

class PatronController extends Controller
{
    public function index()
    {
        return response()->json(Patron::all());
    }
public function store(PatronRequest $request)
    {
        return response()->json(Patron::create($request->validated()));
    }
public function show($id)
    {
        try{
        return response()->json(Patron::findOrFail($id));
    }catch(ModelNotFoundException $exception){
        return response()->json(['msg' => 'Patron not found']);
    }
}
public function update(Request $request, $id)
    {
        $patron = Patron::where('id', $id)->firstOrFail();
        $patron->update($request->all());
        
        return response()->json(['message' => 'Patron updated','patron'=>$patron]);
    }
public function destroy($id)
    {
        $patron = Patron::where('id', $id)->firstOrFail();
        $patron->delete();

        return response()->json(['message' => 'Patron deleted successfully!']);
        
    }
}

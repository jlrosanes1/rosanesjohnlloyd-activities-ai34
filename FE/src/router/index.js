import Vue from 'vue'
import VueRouter from 'vue-router'
import Dashboard from '@/components/pages/index/Index'
import Books from '@/components/pages/books/Books'
import Patron from '@/components/pages/patron/Patron'
import Settings from '@/components/pages/settings/Settings'


Vue.use(VueRouter)

const routes = [{
        path: '/',
        name: 'Dashboard',
        component: Dashboard,
    },
    {
        path: '/books',
        name: 'Books',
        component: Books,
    },
    {
        path: '/patron',
        name: 'Patron',
        component: Patron,
    },
    {
        path: '/settings',
        name: 'Settings',
        component: Settings,
    },
]

const router = new VueRouter({
    mode: 'history',
    routes

})

export default router
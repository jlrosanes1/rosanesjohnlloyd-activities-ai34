var pie = document.getElementById('pieChart').getContext('2d');
var pieChart = new Chart(pie, {
    type: 'doughnut',
    data: {
        datasets: [{
            data: [10, 20, 30, 20],
            backgroundColor: [
                'rgb(94, 130, 15, 1)',
                'rgba(67, 144, 112, 1)',
                'rgba(21, 112, 74, 1)',
                'rgba(21, 57, 42, 1)',

            ],
        }],

        labels: [

            'Biography',
            'Fiction',
            'Novel',
            'Horror',
        ]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var ctx = document.getElementById('barChart').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['Jan', 'FEeb', 'March', 'April', 'May', 'June', 'July', 'Aug', 'Sep', 'Oct'],
        datasets: [{
                label: 'Borrowed Books',
                data: [2, 3, 1, 6, 7, 10, 9, 12, 13, 6],
                backgroundColor: 'rgba(41,77,36,1)',
            },
            {
                label: 'Returned Books',
                data: [1, 2, 1, 1, 4, 7, 10, 11, 8, 8],
                backgroundColor: 'rgba(144,202,81,1)',
            }
        ],
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});